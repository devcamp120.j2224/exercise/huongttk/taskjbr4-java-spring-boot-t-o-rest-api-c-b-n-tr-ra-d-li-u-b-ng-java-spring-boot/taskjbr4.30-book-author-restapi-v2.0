package com.devcamp.jbr4s30.bookauthor;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookAuthorController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList <Book> getBookList(){
        ArrayList <Author> listAuthor1 = new ArrayList<>();
        ArrayList <Author> listAuthor2 = new ArrayList<>();
        ArrayList <Author> listAuthor3 = new ArrayList<>();
        Author author1 = new Author("Huong", "huong@gmail.com", 'F');
        Author author2 = new Author("Toan", "toan@gmail.com", 'M');
        Author author3 = new Author("Hung", "hung@gmail.com", 'M');
        Author author4 = new Author("An", "an@gmail.com",'M');
        Author author5 = new Author("Van", "van@gmail.com", 'F');
        Author author6 = new Author("Phuong", "phuong@gmail.com", 'F');

        listAuthor1.add(author1);
        listAuthor1.add(author2);
        listAuthor2.add(author3);
        listAuthor2.add(author4);
        listAuthor3.add(author5);
        listAuthor3.add(author6);

        ArrayList <Book> listBook = new ArrayList<>();
        Book book1 = new Book("Conan", listAuthor1, 3000, 5);
        Book book2 = new Book("Hat giong tam hon", listAuthor3, 3000, 5);
        Book book3 = new Book("Thuy thu mat trang", listAuthor2, 1000, 10);

        listBook.add(book1);
        listBook.add(book2);
        listBook.add(book3);

        return listBook;
    }
}
