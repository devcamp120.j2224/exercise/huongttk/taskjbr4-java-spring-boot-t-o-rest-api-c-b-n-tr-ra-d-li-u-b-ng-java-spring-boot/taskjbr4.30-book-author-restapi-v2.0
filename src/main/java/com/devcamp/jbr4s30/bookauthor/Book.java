package com.devcamp.jbr4s30.bookauthor;

import java.util.ArrayList;
import java.util.Arrays;

public class Book {
    private String name = "";
    private ArrayList<Author> author = new ArrayList<Author>();
    private double price = 0.0;
    private int qty = 0;

    public Book(String name, ArrayList<Author> author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Author> getAuthor() {
        return author;
    }
    public void setAuthor(ArrayList<Author> author) {
        this.author = author;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<String> getAuthorName() {
        ArrayList<String> authorName = new ArrayList<>();
        for (Author author : this.author) {
            System.out.println(author);
            authorName.add(author.getName());
        }
        return authorName;
    }
    @Override
    public String toString() {
        return "Book [author=" + author + ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
    }

}
